**Generating release signing keys**
            
Keys need to be generated for resigning completed builds from the publicly available test keys. The keys must then be reused for subsequent builds and cannot be changed without flashing the generated factory images again which will perform a
factory reset. Note that the keys are used for a lot more than simply verifying updates and verified boot.

The keys should not be given passwords due to limitations in the upstream scripts. If you want to secure them at rest, you should take a different approach where they can still be available to the signing scripts as a directory of unencrypted keys. The sample certificate subject can be replaced with your own information or simply left as-is.

**Android Verified Boot 1.0**

The Pixel and Pixel XL use Android Verified Boot 1.0

For the first generation Pixel (sailfish) and Pixel XL (marlin), signed releases require building the verity public key into the kernel, so this needs to be done before building the kernel.

Android Verified Boot 1.0
To generate keys for marlin (you should use unique keys per device variant)

    mkdir -p keys/marlin
    cd keys/marlin
    ../../development/tools/make_key releasekey '/CN=AOSP/'
    ../../development/tools/make_key platform '/CN=AOSP/'
    ../../development/tools/make_key shared '/CN=AOSP/'
    ../../development/tools/make_key media '/CN=AOSP/'
    ../../development/tools/make_key verity '/CN=AOSP/'

Upgrading to Android 10
            
For Android 10, there's a new networkstack key and you need to generate one to avoid using the test key. For example, with crosshatch (needs to be done for each set of device keys):

    ../../development/tools/make_key networkstack '/CN=AOSP/'
    cd ../..
        
Generate the verity public key

    make -j20 generate_verity_key
    out/host/linux-x86/bin/generate_verity_key -convert keys/marlin/verity.x509.pem keys/marlin/verity_key

Generate verity keys in the format used by the kernel for the Pixel and Pixel XL

    openssl x509 -outform der -in keys/marlin/verity.x509.pem -out kernel/google/marlin/verifiedboot_marlin_relkeys.der.x509

The same kernel and device repository is used for the Pixel and Pixel XL. There's no separate sailfish kernel


**Android Verified Boot 2.0**

The Pixel 2, Pixel 2 XL, Pixel 3, Pixel 3 XL, Pixel 3a and Pixel 3a XL use Android Verified Boot 2.0 (AVB). Follow the appropriate instructions below

To generate keys for crosshatch (you should use unique keys per device variant):

    mkdir -p keys/crosshatch
    cd keys/crosshatch
    ../../development/tools/make_key releasekey '/CN=AOSP/'
    ../../development/tools/make_key platform '/CN=AOSP/'
    ../../development/tools/make_key shared '/CN=AOSP/'
    ../../development/tools/make_key media '/CN=AOSP/'
    
    openssl genrsa -out avb.pem 2048
    ../../external/avb/avbtool extract_public_key --key avb.pem --output avb_pkmd.bin

The avb_pkmd.bin file isn't needed for generating a signed release but rather to set the public key used by the device to enforce verified boot.

Upgrading to Android 10

For Android 10, there's a new networkstack key and you need to generate one to avoid using the test key. For example, with crosshatch (needs to be done for each set of device keys):

    ../../development/tools/make_key networkstack '/CN=AOSP/'
    cd ../..
